<?php
/**
 * Created by PhpStorm.
 * User: libin
 * Date: 17/7/25
 * Time: 下午2:58
 */

namespace app\models;

use Yii;
use yii\base\Model;

class NewsForm extends Model
{
    public $title;
    public $content;
    public $verifyCode;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['title', 'content'], 'required'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }
}
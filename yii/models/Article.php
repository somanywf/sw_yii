<?php
/**
 * Created by PhpStorm.
 * User: atfeng
 * Date: 17/7/25
 * Time: 上午11:20
 */
namespace app\models;

use yii\db\ActiveRecord;

class Article extends ActiveRecord
{
    public $verifyCode;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content','createtime'], 'string'],
            [['status', 'modified'], 'integer'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '编号',
            'title' => '标题',
            'content' => '内容',
            'status' => '状态',
            'createtime' => '创建时间',
            'modified' => '修改时间',
            'verifyCode' => '验证码'
        ];
    }
    public function beforeSave($insert)
    {
        $timestamp = time();
        if (parent::beforeSave($insert)) {
            if($insert) {
                $this->createtime = $timestamp;
                $this->modified = $timestamp;
                $this->status = 1;
            }
            return true;
        } else {
            return false;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: libin
 * Date: 17/7/25
 * Time: 下午2:58
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
class ArticleForm extends ActiveRecord
{
    public $title;
    public $content;
    public $status;
    public $creattime;
    public $modified;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }
    /**
     * @return array
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['title', 'content'], 'required'],
            [['title'],'string','max'=>100],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'title' => '文章标题',
            'content' => '文章内容',
            'status'=> '状态',
            'verifyCode' => '验证码',
        ];
    }
}
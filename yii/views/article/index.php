<?php
/**
 * Created by PhpStorm.
 * User: atfeng
 * Date: 17/7/25
 * Time: 上午11:30
 */

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title='文章列表';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
        <div style="clear:both;float:left;">
            <?= Html::a('添加', ['create'], ['class' => 'btn btn-success', 'title'=>'添加文章']) ?>
        </div>
            <div style="clear:both;"></div>
            <?php foreach ($rows as $k=>$v): ?>
                <div class="col-lg-4">
                    <h2><?=$v->title?></h2>
                    <h5>[<?=$v->status?'<font color="green"><b>有效</b></font>':'<font color="red"><b>无效</b></font>'?>]</h5>
                    <p><?=$v->content?></p>
                    <p>修改：<?=date('Y-m-d H:i:s',$v->modified)?> <br />创建：<?=date('Y-m-d H:i:s',$v->createtime)?></p>
                    <p>
                        <?= Html::a('编辑', ['edit', 'id' => $v->id], ['class' => 'btn btn-default', 'title'=>'编辑文章']) ?>
                        <?= Html::a('删除', ['delete', 'id' => $v->id],
                            ['class' => 'btn btn-danger',
                                'title'=>'删除文章',
                                'data' => [
                                    'confirm' => '确定要删除该条信息',
                                    'method' => 'post',
                            ]
                            ]) ?>
                        <?= Html::a('查看', ['view', 'id' => $v->id], ['class' => 'btn btn-primary','title'=>'查看文章']) ?>
                    </p>
                </div>
                <?if(($k+1)%3==0){?>
                    <div style="clear:both;"></div>
                <?}?>
            <?php endforeach; ?>
        </div>
        <div style="float:right;"><?= LinkPager::widget(['pagination' => $pagination]) ?></div>

    </div>
</div>

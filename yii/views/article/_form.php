<?php
/**
 * Created by PhpStorm.
 * User: libin
 * Date: 17/7/27
 * Time: 下午3:28
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use dosamigos\datepicker\DatePicker;
?>
<div class="row">
    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(['id' => 'article-form', 'method' => 'post']); ?>
        <? if (isset($row->id) && !empty($row->id)) {
            echo $form->field($model, 'id')->hiddenInput()->label('');
        } ?>

        <?= $form->field($model, 'title')
            ->hint(\Yii::t('app', '请准确填写文章标题'))
            ->textInput(['autofocus' => true])
        ?>

        <?= $form->field($model, 'content')
            ->hint(\Yii::t('app', '请完整填写文章内容'))
            ->textarea(['rows' => 6])
        ?>
        <?= DatePicker::widget([
            'language' => 'zh-CN',
            'model' => $model,
            'attribute' => 'createtime',
            'template' => '{addon}{input}',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]); ?>

        <?= $form->field($model, 'status')->dropdownList([1 => '有效', 0 => '无效']) ?>
        <div class="form-group">
            <?= Html::submitButton('提交', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
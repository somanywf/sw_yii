<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Article', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('更新', ['edit', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定要删除该条信息',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'content:ntext',
            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=> function($data){
                    switch ($data->status) {
                        case '1';
                            return '<font color="green"><b>有效</b></font>';
                            break;
                        default:
                            return '<font color="red"><b>无效</b></font>';
                            break;
                    }
                }
            ],
            [
                'attribute'=>'createtime',
                'format'=>'raw',
                'value'=>function($data){
                    return date('Y 年 m 月 d 日 H:i:s',$data->createtime);
                }
            ],
            'createtime:datetime',
            'modified:datetime',
        ],
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;

$this->title = '编辑【 '.$model->title.' 】';
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
        <div class="alert alert-success">
            Edit, and Push. Enjoy Yourself!
        </div>
        <p>
            享受编辑新闻信息内容的感觉。
        </p>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
</div>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;

$this->title = '添加文章';
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>

        <p>
完成内容编辑。
        </p>

    <?php else: ?>

        <p>
            编辑新闻信息内容。
        </p>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    <?php endif; ?>
</div>

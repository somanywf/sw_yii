<?php
/**
 * Created by PhpStorm.
 * User: libin
 * Date: 17/7/27
 * Time: 上午10:30
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Article;
use app\models\ArticleForm;
use yii\web\NotFoundHttpException;

class ArticleController extends Controller
{
    /**
     * 文章列表
     * @return string
     */
    public function actionIndex(){
        $query = Article::find();

        $pagination = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        $news = $query->orderBy('id DESC')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'rows' => $news,
            'pagination' => $pagination,
        ]);
    }

    public function view(){

    }
    /**
     * 添加文章
     * @return string|\yii\web\Response
     */
    public function actionCreate(){
        $request = Yii::$app->request;
        $model = new Article();
        if($request->isPost){//提交信息
            $post=$request->post();
            $rs_load = $model->load($post);
            $id = $model->save();
            if($rs_load && $id){
                return $this->redirect(['index']);
            }else{
                echo 1;
            }
        }else{//初次访问编辑页面
            return $this->render('create',['model'=>$model]);
        }
    }

    /**
     * 编辑文章
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionEdit($id){
        $request = Yii::$app->request;
        $model =Article::findOne($id);
        if(!$id){
            throw new NotFoundHttpException('不存在该ID');
        }
        if($request->isPost){
            $post = $request->post();
            $post['Article']['createtime'] = (string)strtotime($post['Article']['createtime'].' '.date('H:i:s'));
            $rs_load = $model->load($post);
            $model->modified = time();
            $rs = $model->save();

            if($rs_load && $rs){
                return $this->redirect(['view','id'=>$model->id]);
            }else{
                return $this->index();
            }
        }
        $model->createtime = date('Y-m-d H:i:s',$model->createtime);
        return $this->render('edit',['model'=>$model]);
    }

    public function actionView($id){
        $model = Article::findOne($id);
        return $this->render('view',['model'=>$model]);
    }
    /**
     * 删除文章
     * @return \yii\web\Response
     */
    public function actionDelete(){
        $request = Yii::$app->request;
        $id = $request->get('id');
        if($id){
            Article::findOne($id)->delete();
        }
        return $this->redirect(['index']);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: atfeng
 * Date: 17/7/25
 * Time: 上午11:26
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\News;
use app\models\NewsForm;

class NewsController extends Controller
{
    //展示新闻列表
    public function actionIndex(){
        $query = News::find();

        $pagination = new Pagination([
            'defaultPageSize' => 2,
            'totalCount' => $query->count(),
        ]);

        $news = $query->orderBy('id DESC')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'rows' => $news,
            'pagination' => $pagination,
        ]);
    }

    // 添加/编辑 新闻
    public function actionEdit(){
        $request = Yii::$app->request;
        $model = new NewsForm();
        $query = News::find();
        if($request->isPost){//提交信息
            $post=$request->post();

            $data = [
                'title'=>$post['NewsForm']['title'],
                'content'=>$post['NewsForm']['content'],
                'creattime'=>time(),
                'modified'=>time()
            ];

            /*if(isset($post['NewsForm']['id']) && !empty($post['NewsForm']['id'])){
                unset($data['creattime']);
                $rs = $query->where(['id'=>$post['NewsForm']['id']])->setAttributes($data)->save();
            }else{
                $id = $query->setAttributes($data)->save();
            }*/


        }else{//初次访问编辑页面
            $id = $request->get('id',0);
            $row = $query->where(['id'=>$id])->one();
            return $this->render('edit',['row'=>$row,'model'=>$model]);
        }

    }
}
-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `y_news`;
CREATE TABLE `y_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `status` int(2) DEFAULT '1' COMMENT '状态1有效，0无效',
  `creattime` int(10) DEFAULT '0' COMMENT '创建时间，时间戳',
  `modified` int(10) DEFAULT '0' COMMENT '修改时间，时间戳',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `y_news` (`id`, `title`, `content`, `status`, `creattime`, `modified`) VALUES
(1,	'第一条新闻',	'新闻内容，这块空间比较大',	1,	0,	0),
(2,	'红红火火的新时期',	'咋么回事，这小事，那还不EASY',	1,	0,	0),
(3,	'热烈庆祝上个项目竣工',	'世人笑我太疯癫，我笑世人看不穿！',	1,	1500949320,	1500949320),
(4,	'一条大河~向东流',	'再给我一分钟的时间，送你离开，潜力之外',	1,	1500970920,	1500970920);

-- 2017-07-25 09:55:17
